package example1;

import jdk.nashorn.internal.parser.JSONParser;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Main {
    public static void main(String[] args) {
        // Serialization
        /* Person person=new Person("SaiRam",21,"Andhra");
        try{
            //Storing the Byte into File
            FileOutputStream fileOutputStream=new FileOutputStream("example.txt");
            ObjectOutputStream objectOutputStream=new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(person);
        }
        catch (Exception exception){
            System.out.println(exception.getMessage());
        }*/

        // Deserialization
        try{
            FileInputStream fileInputStream=new FileInputStream("G:\\Intellij projects\\SerializableExample\\example.txt");
            ObjectInputStream objectInputStream=new ObjectInputStream(fileInputStream);
            Person person=(Person) objectInputStream.readObject();
            System.out.println("executed");
        }
        catch (Exception exception){
            System.out.println(exception.getMessage()+"Exception");
        }
    }
}
